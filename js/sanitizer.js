function displayColor(){
  var color = getQueryVariable('color');
  if (color != "") {
    var result = document.getElementById('form_color');
    switch (mode){
      case "RGB":
        result.value = hextoRGB(color);
        break;
      case "RGBA":
        result.value = hextoRGB(color);
        break;
      default:
        result.value = "#"+color;
        break;

    }
  };
}


function getQueryVariable(variable)
{
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){
      return pair[1].toUpperCase();
    }
  }
  return(false);
}


function submitColor(modeconf){
  var inputColor = document.getElementById('form_color').value.replace(/( |%20)+/g, "");
  var query = "index.html?";
  var inferred = inferColorFormat(inputColor);
  console.log(modeconf);
  var color = inferred["color"];

  if(modeconf == null){
    modeconf = inferred["modeconf"];
  }

  window.location = query + "color=" + color.toUpperCase() + "&mode=" + modeconf;
}


function inferColorFormat(inputColor){
  var result = new Array();

  var regexRGB1  = /rgb\((\d{0,3}),(\d{0,3}),(\d{0,3})\)/i;
  var regexRGB2  = /\((\d{0,3}),(\d{0,3}),(\d{0,3})\)/i;
  var regexRGB3  = /(\d{0,3}),(\d{0,3}),(\d{0,3})/i;

  var regexRGBA1 = /rgba\((\d{0,3}),(\d{0,3}),(\d{0,3}),(\d\.\d+)\)/i;
  var regexRGBA2 = /\((\d{0,3}),(\d{0,3}),(\d{0,3}),(\d\.\d+)\)/i;
  var regexRGBA3 = /(\d{0,3}),(\d{0,3}),(\d{0,3}),(\d\.\d+)/i;

  var regexHex1  = /#([A-Z0-9]){6}/i;
  var regexHex2  = /([A-Z0-9]){6}/i;

  var match1 = regexHex1.exec(inputColor);
  var match2 = regexHex2.exec(inputColor);
  var match3;
  if(match1!=null || match2!=null || match3!=null){
    if(match1 !=null){
      color = match1[0].substring(1);
    }
    else{
      color = match2[0];
    }
    modeconf = "hex";
  }
  else{
    match1 = regexRGB1.exec(inputColor);
    match2 = regexRGB2.exec(inputColor);
    match3 = regexRGB3.exec(inputColor);
    if(match1!=null || match2!=null || match3!=null){
      if(match1 != null){
        color = rgbToHex(match1[1],match1[2],match1[3]);
      }
      else if(match2 != null){
        color = rgbToHex(match2[1],match2[2],match2[3]);
      }
      else{
        color = rgbToHex(match3[1],match3[2],match3[3]);
      }
      modeconf = "rgb";
    }
    else {
      match1 = regexRGBA1.exec(inputColor);
      match2 = regexRGBA2.exec(inputColor);
      match3 = regexRGBA3.exec(inputColor);
      if(match1!=null || match2!=null || match3!=null){
        if(match1 != null){
          color = rgbToHex(match1[1],match1[2],match1[3]);
        }
        else if(match2 != null){
          color = rgbToHex(match2[1],match2[2],match2[3]);
        }
        else{
          color = rgbToHex(match3[1],match3[2],match3[3]);
        }
        modeconf = "rgba";
      }
    }
  }

  result["modeconf"] = modeconf;
  result["color"] = color;

  return result;
}


window.addEventListener('load',displayColor);
document.getElementById("form_submit").addEventListener('click',submitColor);
// document.getElementById("form_id").addEventListener('submit',submitColor);
