function calculateColors(hexa) {
  triadicColors(hexa);
  complementaryColors(hexa);
  shadesColors(hexa);
  tintsColors(hexa);
}


function triadicColors(hexa) {
  var hexafirst = hexa.substring(0,2);
  var hexasecond = hexa.substring(2,4);
  var hexathird = hexa.substring(4,6);
  var nbtriadic = 3;
  var triadic = new Array();

  switch(mode) {
    case "RGB":
    triadic[0] = hextoRGB(hexa);
    triadic[1] = hextoRGB(''+hexathird+hexafirst+hexasecond);
    triadic[2] = hextoRGB('' + hexasecond + '' +hexathird + '' + hexafirst);
    break;
    default:
    triadic[0] = getHexaSyntax(hexafirst + hexasecond + hexathird);
    triadic[1] = getHexaSyntax(hexathird + hexafirst + hexasecond);
    triadic[2] = getHexaSyntax(hexasecond + hexathird + hexafirst);
  }

  for(var i=0; i<nbtriadic; i++){
    $("#triadic").append("<div id=\"triadic" + i + "\"  class=\"col-sm-" + 12/nbtriadic + " cursor-color\"> <div class=\"color-case\" data-clipboard-action=\"copy\" data-clipboard-text=\"" + triadic[i] + "\" onclick=" + "popSuccessCopy(\"" + triadic[i] + "\")" + "><p class=\"copy-text\">Click to copy</p></div><p class=\"color-name\"></p></div>");
    $("#triadic"+ i +" > .color-case").css('background-color', triadic[i]);
    $("#triadic"+ i +" > .color-name").text(triadic[i]);
  }
}

function complementaryColors(hexa) {
  var first = hexToR(hexa);
  var second = hexToG(hexa);
  var third = hexToB(hexa);
  var nbcomplementary=2;
  var complementary = new Array();

  switch(mode) {
    case "RGB":
    complementary[0] = hextoRGB(hexa);
    complementary[1] = getRGBSyntax((255 - first),(255 - second) ,(255 - third));
    break;
    default:
    complementary[0] = getHexaSyntax(hexa);
    complementary[1] = getHexaSyntax(rgbToHex(255 - first,255 - second,255 - third));
  }
  for(var i = 0; i < nbcomplementary; i++){
    $("#complementary").append("<div id=\"complementary" + i + "\"  class=\"col-sm-" + 12/nbcomplementary + " cursor-color\"> <div class=\"color-case\"  data-clipboard-action=\"copy\" data-clipboard-text=\"" + complementary[i] + "\" onclick=" + "popSuccessCopy(\"" + complementary[i] + "\")" + "><p class=\"copy-text\">Click to copy</p></div><p class=\"color-name\"></p></div>");
    $("#complementary"+i+" > .color-case").css('background-color', complementary[i]);
    $("#complementary"+i+" > .color-name").text(complementary[i]);
  }
}


function shadesColors(hexa) {
  var nbshade=7;
  var max = "00";

  var r = hexToR(hexa);
  var g = hexToG(hexa);
  var b = hexToB(hexa);

  var deltar = r / (nbshade - 1);
  var deltag = g / (nbshade - 1);
  var deltab = b / (nbshade - 1);

  for(var i = 0; i < nbshade-1; i++){
    switch(mode) {
      case "RGB":
      var shade = getRGBSyntax(Math.trunc(r-i*deltar),Math.trunc(g-i*deltag),Math.trunc(b-i*deltab));
      break;
      default:
      var shade = getHexaSyntax(rgbToHex(r-i*deltar,g-i*deltag,b-i*deltab));
    }
    $("#shades").append("<div id=\"shades" + i + "\"  class=\"col-sm-" + 12/(nbshade-1) + " cursor-color\"> <div class=\"color-case\" data-clipboard-action=\"copy\" data-clipboard-text=\"" + shade + "\" onclick=" + "popSuccessCopy(\"" + shade + "\")" + "><p class=\"copy-text\">Click to copy</p></div><p class=\"color-name\"></p></div>");
    $("#shades"+ i +" > .color-case").css('background-color', shade);
    $("#shades"+ i +" > .color-name").text(shade);
  }
}


function tintsColors(hexa) {
  var nbtint = 7;
  var max = 255;

  var r = hexToR(hexa);
  var g = hexToG(hexa);
  var b = hexToB(hexa);

  var deltar = (max - r) / (nbtint - 1);
  var deltag = (max - g) / (nbtint - 1);
  var deltab = (max - b) / (nbtint - 1);

  var parent = document.getElementById("tints");

  for(var i = 0; i < nbtint-1; i++){
    switch(mode) {
      case "RGB":
      var tint = getRGBSyntax(Math.trunc(r+i*deltar),Math.trunc(g+i*deltag),Math.trunc(b+i*deltab));
      break;
      default:
      var tint = getHexaSyntax(rgbToHex(r+i*deltar,g+i*deltag,b+i*deltab));
    }
    $("#tints").append("<div id=\"tints" + i + "\"  class=\"col-sm-" + 12/(nbtint-1) + " cursor-color\"> <div class=\"color-case\" data-clipboard-action=\"copy\" data-clipboard-text=\"" + tint + "\" onclick=" + "popSuccessCopy(\"" + tint + "\")" + "><p class=\"copy-text\">Click to copy</p></div><p class=\"color-name\"></p></div>");
    $("#tints"+ i +" > .color-case").css('background-color', tint);
    $("#tints"+ i +" > .color-name").text(tint);
  }
}
